import datetime
import math
import os
import re
import roman
import telebot
import time
from telebot import types

BOT_NAME = ''
TOKEN = None
with open("token.txt") as f:
    TOKEN = f.read().strip()


bot = telebot.TeleBot(TOKEN)
TIMEOUT_CONNECTION = 3

START_MESSAGE = """Посмотрите справку: '/help\' \nИли введите выражение:"""
HELP_MESSAGE = """Bыражение должно быть вида: x + ( v - i ). После любой операции должен стоять пробел!!!"""


@bot.message_handler(commands=['start', 'help'])
def send_start(message):
#Command processing
    print('%s (%s): %s' % (message.chat.first_name, message.chat.username, message.text))
    mssg = None
    if message.text.lower() == '/help':
        mssg = bot.send_message(message.chat.id, HELP_MESSAGE, parse_mode='markdown')
    elif message.text.lower() == '/start':
        mssg = bot.send_message(message.chat.id, START_MESSAGE, parse_mode='markdown')
    if (mssg):
        print('Бот: %s ' % mssg.text)


@bot.message_handler(func=lambda message: True)
def answer_to_user(message):
#User_message processing
    print('%s (%s): %s' % (message.chat.first_name, message.chat.username, message.text))
    mssg = None
    user_message = message.text.lower()
    print(user_message)

    if user_message == 'привет':
        mssg = bot.send_message(message.chat.id, '*Привет, %s*' % (message.chat.first_name), parse_mode='markdown')
    elif user_message == 'помощь':
        mssg = bot.send_message(message.chat.id, HELP_MESSAGE, parse_mode='markdown')
    else:
        user_message = message.text.upper()
        user_message = list(map(str, user_message.split()))
        """The user's message is converted and converted to an Arabic system.
        Then there's the process of operations(+-*/).
        And at the end it is translated back into the Roman system."""
        for i in range(len(user_message)):
            if not user_message[i] in '()//+-*':
                try:
                    user_message[i] = roman.fromRoman(user_message[i])
                except roman.InvalidRomanNumeralError:
                    mssg = bot.send_message(message.chat.id, 'Проверьте введенное выражение')
                    break
                except RecursionError:
                    mssg = bot.send_message(message.chat.id, "Изучите, пожалуйста, справку) \n'/help\'")
                    break
        user_message_to = message.text.upper().strip()
        user_message = "".join(map(str, user_message))
        print(user_message)
        try:
            answer = str(roman.toRoman(int(eval(user_message.replace(' ', '').replace('/', '//')))))
            mssg = bot.send_message(message.chat.id, user_message_to.replace(' ', '') + ' = ' + answer.replace('//', '/'))
#Error processing
        except ZeroDivisionError:
            mssg = bot.send_message(message.chat.id, 'Не надо так. \nТолько не ноль 👉👈')
        except SyntaxError:
            mssg = bot.send_message(message.chat.id, 'Что-то не то. \nИсравьте ошибку')
        except NameError:
            mssg = bot.send_message(message.chat.id, 'Неправильно написано выражение (забыли пробел или вводите неизвестный символ).')
        except roman.NotIntegerError:
            mssg = bot.send_message(message.chat.id, 'Только целые числа!!!')
        except roman.OutOfRangeError:
            mssg = bot.send_message(message.chat.id, '-' + str(roman.toRoman((-1)*int(eval(user_message.replace(' ', ''))))))

    if (mssg):
        print('Бот: %s' % mssg.text)

#Login to the program
if (__name__ == '__main__'):
    while True:
        try:
            bot.polling(none_stop=True)
        except Exception as e:
            print ('Ошибка подключения. Попытка подключения через %s сек.' % TIMEOUT_CONNECTION)
            time.sleep(TIMEOUT_CONNECTION)
